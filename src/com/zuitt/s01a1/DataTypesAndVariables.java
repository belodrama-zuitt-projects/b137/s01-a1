package com.zuitt.s01a1;

public class DataTypesAndVariables {
    public static void main(String[] args){
        //First Name
        String firstName = "Belen";

        //Last Name
        String lastName = "Macaraig";

        //grades
        float gradeEnglish = 95f;
        float gradeMathematics = 92f;
        float gradeScience = 90.5f;

        //average
        float average = (gradeEnglish + gradeMathematics + gradeScience) / 3;

        System.out.println(firstName + " "+ lastName + " has a grade average of " + average + ".");

    }
}
